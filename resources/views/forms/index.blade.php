<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('templates') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
                    <div class="md:grid md:grid-cols md:gap">
                        <div class="md:col-span-1">
                            <x-jet-section-title>
                                <x-slot name="title">
                                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">Tabla de planillas</h2>
                                </x-slot>
                                <x-slot name="description">
                                </x-slot>


                            </x-jet-section-title>
                        </div>
                        <div class="mt-5 md:mt-2 md:col-span-2">
                            <!-- component -->
                            <div class="flex flex-col">
                                <div class="overflow-x-auto shadow-md sm:rounded-lg">
                                    <div class="inline-block min-w-full align-middle">
                                        <livewire:search-templates />                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</x-app-layout>
